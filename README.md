# BBW teachtalk - DevOps für Systemtechniker

## Workflow
Dein lokales Repository besteht aus drei "Instanzen", die von git verwaltet werden.
Die erste ist deine `Arbeitskopie`, welche die echten Dateien enthält.
Die zweite ist der `Index`, welcher als Zwischenstufe agiert und zu guter Letzt noch der `HEAD`, der auf deinen letzten Commit zeigt.

![trees](.assets/trees.png)

## add & commit
Du kannst Änderungen vorschlagen (zum `Index` hinzufügen) mit
```shell
git add <dateiname>
git add *
```

Das ist der erste Schritt im git workflow, du bestätigst deine Änderungen mit:
```shell
git commit -m "Commit-Nachricht"
```
Jetzt befindet sich die Änderung im `HEAD`, aber noch nicht im entfernten Repository.

# Änderungen hochladen
Die Änderungen sind jetzt im `HEAD` deines lokalen Repositories. Um die Änderungen an dein entferntes Repository zu senden, führe folgenen Befehl aus:
```shell
git push origin master
```
aus. Du kannst master auch mit einem beliebigen anderen Branch ersetzen.
## Branching
Branches werden benutzt, um verschiedene Funktionen isoliert voneinander zu entwickeln. Der master-Branch ist der "Standard"-Branch, wenn du ein neues Repository erstellst. Du solltest aber für die Entwicklung andere Branches verwenden und diese dann in den Master-Branch zusammenführen (mergen).

![trees](.assets/branches.png)

Erstelle einen neuen Branch mit dem Namen "feature_x" und wechsle zu diesem:
```shell
git checkout -b feature_x
```
Um zum Master zurück zu wechseln:
```shell
git checkout master
```
Und um den eben erstellten Branch wieder zu löschen:
```shell
git branch -d feature_x
```
Ein Branch ist nicht für andere verfügbar, bis du diesen in dein entferntes Repository hochlädst:
```shell
git push origin <branch>
```

## update & merge
Um dein lokales Repository mit den neuesten Änderungen zu aktualisieren, verwende:
```shell
git pull
```
in deiner Arbeitskopie, um die Änderungen erst herunterzuladen (fetch) und dann mit deinem Stand zusammenzuführen (merge).

git versucht die Änderungen automatisch zusammenzuführen. Unglücklicherweise ist dies nicht immer möglich und endet in Konflikten. Du bist verantwortlich, diese Konflikte durch manuelles Editieren der betroffenen Dateien zu lösen. Bist du damit fertig, musst du das git mit folgendem Befehl mitteilen:
```shell
git add <dateiname>
```

## Änderungen rückgängig machen
Falls du mal etwas falsch machst kannst du die lokalen Änderungen mit:
```shell
git checkout -- <filename>
```
auf den letzten Stand im `HEAD` zurücksetzen. Änderungen, die du bereits zum `Index` hinzugefügt hast, bleiben bestehen.

Wenn du aber deine lokalen Änderungen komplett entfernen möchtest, holst du dir den letzten Stand vom entfernten Repository mit folgenden Befehlen:
```shell
git fetch origin
git reset --hard origin/master
```

## Aknowledgement
Dieses Einführung basiert auf der Anleitung von Roger Dudler. Herzlichen Dank.
- git - Der einfache Einstieg: https://rogerdudler.github.io/git-guide/index.de.html
